#include <Arduino.h>

//Triangular membership function definition
float trimf(float x, float a, float b, float c);

float speed(float ang, float der);

float ang_neg(float ang);
float ang_zr(float ang);
float ang_pos(float ang);
float der_neg(float ang);
float der_zr(float ang);
float der_pos(float ang);


int ang = -44;
int der = -9;
unsigned long initCount=0;
unsigned long fnshCount=0;

void setup() {
    // put your setup code here, to run once:
    Serial.begin(9600);
    Serial.print("Angle,\t");
    Serial.print("Derivative,\t");
    Serial.print("Speed,\t");
    Serial.println("Time");
    delay(100);
}

void loop() {
    // put your main code here, to run repeatedly:
    initCount = micros();
    float out = speed(ang, der);
    fnshCount = micros();
    
    Serial.print(ang);
    Serial.print("\t");
    Serial.print(der);
    Serial.print("\t");
    Serial.print(out, 3);
    Serial.print("\t");
    Serial.println(fnshCount-initCount);
    
    ang++;
    if (ang >= 45){
      ang = -44;
      der++;
      if (der >= 10) {
        der = -9;
        delay(100000);      
      }
    }
}

// Function for calculating aterniation to a membership function
  // x = our variable (measurment of angle or derivative)
  // a = starting point of triangle
  // b = peak of triangle
  // c = ending point of triangle
float trimf(float x,float a, float b, float c){
  float f;
  if(x<=a)
     f=0;
  else if((a<=x)&&(x<=b)) 
     f=(x-a)/(b-a);
  else if((b<=x)&&(x<=c)) 
     f=(c-x)/(c-b);
  else if(c<=x) 
     f=0;
  return f; 
}

/*******************************************************
 * Functions to compute pertinence of an input to 
 * a fuzzy set
 * ****************************************************/

float ang_neg(float ang){
    return trimf(ang, -45, -44.9, 0);
}

float ang_zr(float ang){
    return trimf(ang, -30, 0, 30);
}

float ang_pos(float ang){
    return trimf(ang, 0, 44.9, 45);
}

float der_neg(float ang){
    return trimf(ang, -10, -9.9, 0);
}

float der_zr(float ang){
    return trimf(ang, -5, 0, 5);
}

float der_pos(float ang){
    return trimf(ang, 0, 9.9, 10);
}

// Function to control robot speed based on angle and
// its derivative

float speed(float ang, float der){

    float out = 0.0;
    if (ang>60)  ang =  45;
    if (ang<-60) ang = -45;
    if (der>10)  der =  10;
    if (der<-10) der = -10;
    // The output universe has its fuzzy sets on singleton
    // form for simplicity
    float negFast = -1.0;
    float negSlow = -0.5;
    float stopped =  0.0;
    float posSlow =  0.5;
    float posFast =  1.0;

    /***************************************************
     * Fuzzy Rules definition
     **************************************************/

    // Rule ANT1: IF (ang is neg AND der is neg) OR  
    // (ang is neg AND der is zr) THEN posFast
    float r1 = max(min(ang_neg(ang), der_neg(der)), 
                   min(ang_neg(ang), der_zr(der)));
    
    // Rule ANT2: IF (ang is pos AND der is pos) OR 
    // (ang is pos AND der is zr) THEN negFast
    float r2 = max(min(ang_pos(ang), der_pos(der)), 
                   min(ang_pos(ang), der_zr(der)));

    // Rule ANT3: IF (ang is neg AND der is pos) OR 
    // (ang is zr AND der is neg) THEN posSlow
    float r3 = max(min(ang_neg(ang), der_pos(der)), 
                   min(ang_zr(ang), der_neg(der)));
    
    // Rule ANT4: IF (ang is pos AND der is neg) OR 
    // (ang is zr AND der is pos) THEN negSlow
    float r4 = max(min(ang_pos(ang), der_neg(der)), 
                   min(ang_zr(ang), der_pos(der)));

    // Rule ANT5: IF (ang is zr AND der is zr) 
    // THEN stopped
    float r5 = min(ang_neg(ang), der_neg(der));

    // Deffuzyfication
    out = r1*posFast + r2*negFast + r3*posSlow + 
          r4*negSlow + r5*stopped;
           
    return out;    
}
