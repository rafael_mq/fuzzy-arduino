#include <FuzzyComposition.h>
#include <Fuzzy.h>
#include <FuzzyRule.h>
#include <FuzzySet.h>
#include <FuzzyRuleAntecedent.h>
#include <FuzzyRuleConsequent.h>
#include <FuzzyIO.h>
#include <FuzzyOutput.h>
#include <FuzzyInput.h>

// Fuzzy Object
Fuzzy* fuzzy = new Fuzzy();

// Fuzzy sets for the antecedent (angle measurement) universe
FuzzySet* angNeg = new FuzzySet(-60, -60, -45, -15);
FuzzySet* angZr  = new FuzzySet(-15,   0,   0,  15);
FuzzySet* angPos = new FuzzySet(15,   45,  60,  60);

// Fuzzy set for the antecedent (derivative of angle) universe
FuzzySet* derNeg = new FuzzySet(-100, -100, -50,   0);
FuzzySet* derZr  = new FuzzySet( -50,    0,   0,  50);
FuzzySet* derPos = new FuzzySet(  50,  100, 100, 100); 

void setup(){

  Serial.begin(9600); 

  // Fuzzy Input angle
  FuzzyInput* angle = new FuzzyInput(1);
  angle->addFuzzySet(angNeg);
  angle->addFuzzySet(angZr);
  angle->addFuzzySet(angPos);

  fuzzy->addFuzzyInput(angle);
  
  // Fuzzy Input angle derivative
  FuzzyInput* derivative = new FuzzyInput(2);
  derivative->addFuzzySet(derNeg);
  derivative->addFuzzySet(derZr);
  derivative->addFuzzySet(derPos);

  fuzzy->addFuzzyInput(derivative);
  
  // Fuzzy Output
  FuzzyOutput* speed = new FuzzyOutput(1);
  //Fuzzy set for the consequent (motor speed) universe
  FuzzySet* negFast = new FuzzySet(-255, -255, -255, -127);
  speed->addFuzzySet(negFast);
  FuzzySet* negSlow = new FuzzySet(-255, -127, -127,    0);
  speed->addFuzzySet(negSlow);
  FuzzySet* stopped = new FuzzySet(-127,    0,    0,  127);
  speed->addFuzzySet(stopped);
  FuzzySet* posSlow = new FuzzySet(   0,  127,  127,  255);
  speed->addFuzzySet(posSlow);
  FuzzySet* posFast = new FuzzySet( 127,  255,  255,  255);
  speed->addFuzzySet(posFast);

  fuzzy->addFuzzyOutput(speed);

  /*****************************************************************************
      Fuzzy Rule 1
  *****************************************************************************/

  // IF angle is negative AND derivative is negative
  FuzzyRuleAntecedent* anguloNegAndDerivativeNeg = new FuzzyRuleAntecedent();
  anguloNegAndDerivativeNeg->joinWithAND(angNeg, derNeg);

  // OR angle is negative AND derivative is zero
  FuzzyRuleAntecedent* anguloNegAndDerivativeZero = new FuzzyRuleAntecedent();
  anguloNegAndDerivativeZero->joinWithAND(angNeg,derZr);

  FuzzyRuleAntecedent* angNegAndDerNegOrZero = new FuzzyRuleAntecedent();
  angNegAndDerNegOrZero -> joinWithOR(anguloNegAndDerivativeNeg, anguloNegAndDerivativeZero);

  // THEN speed is posFast
  FuzzyRuleConsequent* thenSpeedPosFast = new FuzzyRuleConsequent();
  thenSpeedPosFast->addOutput(posFast);

  FuzzyRule* fuzzyRule1 = new FuzzyRule(1, angNegAndDerNegOrZero, thenSpeedPosFast);
  fuzzy->addFuzzyRule(fuzzyRule1);

  /*****************************************************************************
      Fuzzy Rule 2
  *****************************************************************************/

  // IF angle is positive AND derivative is positive
  FuzzyRuleAntecedent* anguloPosAndDerivativePos = new FuzzyRuleAntecedent();
  anguloPosAndDerivativePos->joinWithAND(angPos,derNeg);

  // OR angle is positive AND derivative is zero
  FuzzyRuleAntecedent* anguloPosAndDerivativeZero = new FuzzyRuleAntecedent();
  anguloPosAndDerivativeZero->joinWithAND(angPos,derZr);

  FuzzyRuleAntecedent* angPosAndDerPosOrZero = new FuzzyRuleAntecedent();
  angPosAndDerPosOrZero->joinWithOR(anguloPosAndDerivativePos, anguloPosAndDerivativeZero);

  // THEN speed is negFast
  FuzzyRuleConsequent* thenSpeedNegFast = new FuzzyRuleConsequent();
  thenSpeedNegFast->addOutput(negFast);

  FuzzyRule* fuzzyRule2 = new FuzzyRule(2, angPosAndDerPosOrZero, thenSpeedNegFast);
  fuzzy->addFuzzyRule(fuzzyRule2);

  /*****************************************************************************
      Fuzzy Rule 3
  *****************************************************************************/

  // IF angle is zero AND derivative is negative
  FuzzyRuleAntecedent* angZrAndDerNeg = new FuzzyRuleAntecedent();
  angZrAndDerNeg->joinWithAND(angZr, derNeg);

  // OR angle is negative AND derivative is positive
  FuzzyRuleAntecedent* angNegAndDerPos = new FuzzyRuleAntecedent();
  angNegAndDerPos->joinWithAND(angNeg, derPos);

  FuzzyRuleAntecedent* Antec3 = new FuzzyRuleAntecedent();
  Antec3->joinWithOR(angZrAndDerNeg, angNegAndDerPos);
  
  // THEN speed is negSlow
  FuzzyRuleConsequent* thenSpeedPosSlow = new FuzzyRuleConsequent();
  thenSpeedPosSlow->addOutput(posSlow);

  FuzzyRule* fuzzyRule3 = new FuzzyRule(3, Antec3, thenSpeedPosSlow);
  fuzzy->addFuzzyRule(fuzzyRule3);

  /*****************************************************************************
      Fuzzy Rule 4
  *****************************************************************************/

  // IF angle is zero AND derivative is positive
  FuzzyRuleAntecedent* angZrAndDerPos = new FuzzyRuleAntecedent();
  angZrAndDerPos->joinWithAND(angZr, derNeg);

  // OR angle is positive AND derivative is negative
  FuzzyRuleAntecedent* angPosAndDerNeg = new FuzzyRuleAntecedent();
  angPosAndDerNeg->joinWithAND(angNeg, derPos);

  FuzzyRuleAntecedent* Antec4 = new FuzzyRuleAntecedent();
  Antec4->joinWithOR(angZrAndDerPos, angPosAndDerNeg);

  // THEN speed is negSlow
  FuzzyRuleConsequent* thenSpeedNegSlow = new FuzzyRuleConsequent();
  thenSpeedNegSlow->addOutput(posSlow);

  FuzzyRule* fuzzyRule4 = new FuzzyRule(4, Antec4, thenSpeedNegSlow);
  fuzzy->addFuzzyRule(fuzzyRule4);

  /*****************************************************************************
      Fuzzy Rule 5
  *****************************************************************************/
  
  // IF angle is zero AND derivative is zero
  FuzzyRuleAntecedent* angZrAndDerZr = new FuzzyRuleAntecedent();
  angZrAndDerZr->joinWithAND(angZr, derZr);  

  // THEN speed is zero
  FuzzyRuleConsequent* thenSpeedZr = new FuzzyRuleConsequent();
  thenSpeedZr->addOutput(stopped);

  FuzzyRule* fuzzyRule5 = new FuzzyRule(5, angZrAndDerZr, thenSpeedZr);
  fuzzy->addFuzzyRule(fuzzyRule5) ; 
}

void loop(){
  fuzzy->setInput(1, -30);
  fuzzy->setInput(2, 50);

  fuzzy->fuzzify();
  float output = fuzzy->defuzzify(1);
  Serial.println(output);

  delay(100);
}
