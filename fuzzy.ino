#include <FuzzyComposition.h>
#include <Fuzzy.h>
#include <FuzzyRule.h>
#include <FuzzySet.h>
#include <FuzzyRuleAntecedent.h>
#include <FuzzyRuleConsequent.h>
#include <FuzzyIO.h>
#include <FuzzyOutput.h>
#include <FuzzyInput.h>

// Fuzzy Object
Fuzzy* fuzzy = new Fuzzy();

// Fuzzy sets for the antecedent (angle measurement) universe
FuzzySet* angNeg = new FuzzySet(-60, -60, -45, -15);
FuzzySet* angZr  = new FuzzySet(-15,   0,   0,  15);
FuzzySet* angPos = new FuzzySet(15,   45,  60,  60);

// Fuzzy set for the antecedent (derivative of angle) universe
FuzzySet* derNeg = new FuzzySet(-100, -100, -50,   0);
FuzzySet* derZr  = new FuzzySet( -50,    0,   0,  50);
FuzzySet* derPos = new FuzzySet(  50,  100, 100, 100);

//Fuzzy set for the consequent (motor speed) universe
FuzzySet* negFast = new FuzzySet(-255, -255, -255, -127);
FuzzySet* negSlow = new FuzzySet(-255, -127, -127,    0);
FuzzySet* stopped = new FuzzySet(-127,    0,    0,  127);
FuzzySet* posSlow = new FuzzySet(   0,  127,  127,  255);
FuzzySet* posFast = new FuzzySet( 127,  255,  255,  255); 

void setup(){

  Serial.begin(9600); 

  // Fuzzy Input angle
  FuzzyInput* angle = new FuzzyInput(1);
  angle->addFuzzySet(angNeg);
  angle->addFuzzySet(angZr);
  angle->addFuzzySet(angPos);

  fuzzy->addFuzzyInput(angle);
  
  // Fuzzy Input angle derivative
  FuzzyInput* derivative = new FuzzyInput(2);
  derivative->addFuzzySet(derNeg);
  derivative->addFuzzySet(derZr);
  derivative->addFuzzySet(derPos);

  fuzzy->addFuzzyInput(derivative);
  
  // Fuzzy Output
  FuzzyOutput* speed = new FuzzyOutput(1);
  speed->addFuzzySet(negFast);
  speed->addFuzzySet(negSlow);
  speed->addFuzzySet(stopped);
  speed->addFuzzySet(posSlow);
  speed->addFuzzySet(posFast);

  fuzzy->addFuzzyOutput(speed);

  /* 
      Fuzzy Rule 1
  */

  // IF angle is negative AND derivative is negative
  FuzzyRuleAntecedent* anguloNegAndDerivativeNeg = new FuzzyRuleAntecedent();
  anguloNegAndDerivativeNeg->joinWithAND(angNeg, derNeg);

  // OR angle is negative AND derivative is zero
  FuzzyRuleAntecedent* anguloNegAndDerivativeZero = new FuzzyRuleAntecedent();
  anguloNegAndDerivativeZero->joinWithAND(angNeg,derZr);

  FuzzyRuleAntecedent angNegAndDerPosOrZero = new FuzzyRuleAntecedent();
  angNegAndDerPosOrZero->joinWithOR(anguloNegAndDerivativePos, anguloNegAndDerivativeZero);

  // THEN speed is posFast
  FuzzyRuleConsequent* thenSpeedPosFast = new FuzzyRuleConsequent();
  thenSpeedPosFast->addOutput(posFast);

  FuzzyRule* fuzzyRule1 = new FuzzyRule(1, angNegAndDerPosOrZero, thenSpeedPosFast);
  fuzzy->addFuzzyRule(fuzzyRule1);

  /* 
      Fuzzy Rule 2
  */

  // IF angle is positive AND derivative is positive
  FuzzyRuleAntecedent* anguloPosAndDerivativePos = new FuzzyRuleAntecedent();
  anguloPosAndDerivativePos->joinWithAND(angPos,derNeg);

  // OR angle is positive AND derivative is zero
  FuzzyRuleAntecedent* anguloPosAndDerivativeZero = new FuzzyRuleAntecedent();
  anguloPosAndDerivativeZero->joinWithAND(angPos,derZr);

  FuzzyRuleAntecedent angPosAndDerPosOrZero = new FuzzyRuleAntecedent();
  angPosAndDerPosOrZero->joinWithOR(anguloPosAndDerivativePos, anguloPosAndDerivativeZero);

  // THEN speed is negFast
  FuzzyRuleConsequent* thenSpeedNegFast = new FuzzyRuleConsequent();
  thenSpeedNegFast->addOutput(negFast);

  FuzzyRule* fuzzyRule2 = new FuzzyRule(2, angPosAndDerPosOrZero, thenSpeedNegFast);
  fuzzy->addFuzzyRule(fuzzyRule2);

  /*
      Fuzzy Rule 3
  */

  // IF angle is zero AND derivative is negative
  // OR angle is negative AND derivative is positive
  // THEN speed is negSlow
  }

void loop(){
  
}
