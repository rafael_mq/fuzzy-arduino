#include <Arduino.h>

// Definição dos pinos do Arduino
const int pin_led = 13,
          pino1_mot_a = 5,
          pino2_mot_a = 6,
          pino1_mot_b = 9,
          pino2_mot_b = 10;
          
//Função: Gerador de funções de pertinência Fuzzy triangulares ou trapezóidais
float trimf(float x, float a, float b, float c, float d);

//Função: Controlador Fuzzy
float speed(float ang, float der);

inline void atua_motor_a(int value){//recebe um valor de -255 a 255
  byte dir=0;
  unsigned char val = value;
  if (value<0){ 
    dir=1; 
    value=-value;
  }
  
  if(value>255)
    val=255;
  else
    val=value;
  
  if(dir==0){    // sentido de z
    analogWrite(pino1_mot_a,val);
    digitalWrite(pino2_mot_a,0);
  }
  else {  // sentido de -z
    digitalWrite(pino1_mot_a,0);
    analogWrite(pino2_mot_a,val);
  }

  if(value > 0){
    
  }
}

inline void atua_motor_b(int value){//recebe um valor de -255 a 255
  byte dir=0;
  unsigned char val = value;
  if (value<0){ 
    dir=1; 
    value=-value;
  }
  
  if(value>255)
    val=255;
  else
    val=value;
  
  if(dir==0){    // sentido de z
    analogWrite(pino1_mot_b,val);
    digitalWrite(pino2_mot_b,0);
  }
  else {  // sentido de -z
    digitalWrite(pino1_mot_b,0);
    analogWrite(pino2_mot_b,val);
  }
}

// Funções: Conjuntos Fuzzy de Entrada
float ang_neg(float ang);
float ang_zr(float ang);
float ang_pos(float ang);
float der_neg(float ang);
float der_zr(float ang);
float der_pos(float ang);

#define PITCH_SP 22.0
float out_fuzzy;
int pwm_motor = 0;

double compAngleY;
double angY;

//Variáveis do sensor
int16_t ax, ay, az, gx, gy, gz;	//Esses vão receber os dados crus do MPU6050
double AcX,AcY,AcZ,GyY;		//Esses valores vão ser usados nos cálculos
uint32_t timer;					//Timer para calcular o atraso de cada iteração.
uint32_t temp;
#define degconvert 57.2957786	//there are like 57 degrees in a radian.

//Bibliotecas do I2C e do sensor MPU6050
#include "I2Cdev.h"
#include "MPU6050_6Axis_MotionApps20.h"

#if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
    #include "Wire.h"
#endif

// Definição do objeto da classe MPU6050
MPU6050 accelgyro;

void setup() {

// Setup da comunicação I2C
  #if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
      Wire.begin();
  #elif I2CDEV_IMPLEMENTATION == I2CDEV_BUILTIN_FASTWIRE
      Fastwire::setup(400, true);
  #endif

  // Inicialização dos pinos do Arduino    
  pinMode(pino1_mot_a,OUTPUT);
  pinMode(pino2_mot_a,OUTPUT);
  pinMode(pino1_mot_b,OUTPUT);
  pinMode(pino2_mot_b,OUTPUT);
  pinMode(pin_led,OUTPUT);

  Serial.begin(115200);

  // initialize device
  Serial.println("Initializing I2C devices...");
  accelgyro.initialize();

  // verify connection
  Serial.println("Testing device connections...");
  Serial.println(accelgyro.testConnection() ? "MPU6050 connection successful" : "MPU6050 connection failed");

  /*********************************************************************************************************
  * PRECISO FAZER UM CODIGO PARA CALIBRAR O SENSOR OU ADD ELE NO SETUP !!!!
  *********************************************************************************************************/

  // Definição dos valores de offset obtidos da calibração
  accelgyro.setXAccelOffset(-518);
  accelgyro.setYAccelOffset(-1095);
  accelgyro.setZAccelOffset(1604);
  accelgyro.setXGyroOffset(101);
  accelgyro.setYGyroOffset(22);
  accelgyro.setZGyroOffset(63);

  //1) Obtenção das medidas do acelerômetro
  accelgyro.getMotion6(&ax, &ay, &az, &gx, &gy, &gz);
  AcX = (double)(ax);
  AcY = (double)(ay);
  AcZ = (double)(az);
  GyY = (double)(gy);

  //2) Cálculo do pitch
  double pitch = atan2(-AcX, AcZ)*degconvert;

  //3) Define o ângulo inicial para os cáculos posteriores
  double gyroYangle = pitch;
  double compAngleY = pitch;

  //start a timer
  timer = micros();
}

void loop() {

  temp = micros();
  
  //1) Obtenção das medidas do acelerômetro
  accelgyro.getMotion6(&ax, &ay, &az, &gx, &gy, &gz);
  AcX = (double)(ax);
  AcY = (double)(ay);
  AcZ = (double)(az);
  GyY = (double)(gy);
  
  double dt = (double)(micros() - timer) / 1000000; //3) Calcula o intervalo de tempo de uma iteração em microsegundos,transforma em 
  													//segundos e invoca o resultado como um valor do tipo "double".
  timer = micros(); //Atualiza o valor da contagem de tempo.

  //3) Cálculo do pitch
  double pitch = atan2(-AcX, AcZ)*degconvert;

  //Converte o dado cru do giroscópio em graus/segundo.
  double gyroYrate = GyY/131.0;

  //FILTRO COMPLEMENTAR
  //Este filtro calcula o ângulo, quase totalmente, integrando a velocidade angular. "dt" é o tempo de amostragem equivalente ao inverso
  //da frequência de amostragem, Fs. O filtro adiciona um fator pequeno equivalente ao ângulo obtido do acelerômetro.
  //A soma dos fatores deve ter valor unitário pra se manter na escala desejada. Note que 0.95 + 0.05 = 1
  compAngleY = 0.9 * (compAngleY + gyroYrate * dt) + 0.1 * pitch; // Calculate the angle using a Complimentary filter.

  if (compAngleY > 0) {
    angY = compAngleY - 180.0;
  }
  else{
    angY = compAngleY + 180.0;
  }

  if ((angY > 60.0) || (angY< -60.0)){
    angY = 0.0;
  }
  out_fuzzy = speed((float)angY, (float)gyroYrate);
  pwm_motor = (int)(out_fuzzy * 255.0);
  atua_motor_a(pwm_motor);
  atua_motor_b(pwm_motor);
  
  temp = micros() - temp;
//  Serial.print("pitch: ");
//  Serial.print(angY);
//  Serial.print(" out:"); Serial.print(pwm_motor);
//  Serial.print(" TP: ");
//  Serial.println(temp);
}

// Função para avaliar a função de pertinência de uma variável fuzzy x
// ao conjunto fuzzy definido pelos valores a, b, c e d
// x = variável fuzzy (medida do ângulo ou sua derivada no tempo)
// a = começo do trapézio
// b = começo do platô
// c = final do plato
// d = final do trapézio
//
//            b     c
//            * * * *
//          *         *
//     a  *             * d
//  * * *                 * * *
float trimf(float x,float a, float b, float c, float d){
  float f;
  if(x<=a)
     f=0.0;
  else if((a<x)&&(x<=b)) 
     f=(x-a)/(b-a);
  else if((b<x)&&(x<=c))
     f=1.0;
  else if((c<x)&&(x<=d)) 
     f=(d-x)/(d-c);
  else if(x>d) 
     f=0.0;
  return f; 
}

/*******************************************************
 * Functions to compute membership value of an input to 
 * a fuzzy set
 ******************************************************/
float ang_neg(float ang){
    return trimf(ang, (-60.0+PITCH_SP), (-59.9+PITCH_SP), (-15.0+PITCH_SP), (-5.0+PITCH_SP));
}

float ang_zr(float ang){
    return trimf(ang, (-15.0+PITCH_SP), (-5.0+PITCH_SP), (5.0+PITCH_SP), (15.0+PITCH_SP));
}

float ang_pos(float ang){
    return trimf(ang, (5.0+PITCH_SP), (15.0+PITCH_SP), (59.9+PITCH_SP), (60.0+PITCH_SP));
}

float der_neg(float der){
    return trimf(der, -100.0, -99.9, -50.0, 0.0);
}

float der_zr(float der){
    return trimf(der, -50.0, 0.0, 0.0, 50.0);
}

float der_pos(float der){
    return trimf(der, 0.0, 50.0, 99.9, 100.0);
}

/*******************************************************
 * Function to control robot speed based on angle and
 * its derivative
 * ****************************************************/
float speed(float ang, float der){

    float out = 0.0;
    if (ang >  60.0+PITCH_SP)  ang =  60.0+PITCH_SP;
    if (ang < -60.0+PITCH_SP)  ang = -60.0+PITCH_SP;
    if (der >  100.0)  der =  100.0;
    if (der < -100.0)  der = -100.0;
    // The output universe has its fuzzy sets on singleton
    // form for simplicity
    float negFast = -1.0;
    float negSlow = -0.5;
    float stopped =  0.0;
    float posSlow =  0.5;
    float posFast =  1.0;

    /***************************************************
     * Fuzzy Rules definition
     **************************************************/

    // Rule ANT1: IF (ang is neg AND der is neg) OR  
    // (ang is neg AND der is zr) THEN posFast
    float r1 = max(min(ang_neg(ang), der_neg(der)), 
                   min(ang_neg(ang), der_zr(der)));
    
    // Rule ANT2: IF (ang is pos AND der is pos) OR 
    // (ang is pos AND der is zr) THEN negFast
    float r2 = max(min(ang_pos(ang), der_pos(der)), 
                   min(ang_pos(ang), der_zr(der)));

    // Rule ANT3: IF (ang is neg AND der is pos) OR 
    // (ang is zr AND der is neg) THEN posSlow
    float r3 = max(min(ang_neg(ang), der_pos(der)), 
                   min(ang_zr(ang), der_neg(der)));
    
    // Rule ANT4: IF (ang is pos AND der is neg) OR 
    // (ang is zr AND der is pos) THEN negSlow
    float r4 = max(min(ang_pos(ang), der_neg(der)), 
                   min(ang_zr(ang), der_pos(der)));

    // Rule ANT5: IF (ang is zr AND der is zr) 
    // THEN stopped
    float r5 = min(ang_neg(ang), der_neg(der));

    // Deffuzyfication
    out = r1*posFast + r2*negFast + r3*posSlow + 
          r4*negSlow + r5*stopped;
           
    return out;    
}
