#include <Arduino.h>

const int pin_led = 13,
          pino1_mot_a = 5,
          pino2_mot_a = 6,
          pino1_mot_b = 9,
          pino2_mot_b = 10;

uint32_t timer;          //Timer para calcular o atraso de cada iteração.
int count = 0;

inline void atua_motor_a(int value){//recebe um valor de -255 a 255
  byte dir=0;
  unsigned char val = value;
  if (value<0){ 
    dir=1; 
    value=-value;
  }
  
  if(value>255)
    val=255;
  else
    val=value;
  
  if(dir==0){    // sentido de z
    analogWrite(pino1_mot_a,val);
    digitalWrite(pino2_mot_a,0);
  }
  else {  // sentido de -z
    digitalWrite(pino1_mot_a,0);
    analogWrite(pino2_mot_a,val);
  }

  if(value > 0){
    
  }
}

inline void atua_motor_b(int value){//recebe um valor de -255 a 255
  byte dir=0;
  unsigned char val = value;
  if (value<0){ 
    dir=1; 
    value=-value;
  }
  
  if(value>255)
    val=255;
  else
    val=value;
  
  if(dir==0){    // sentido de z
    analogWrite(pino1_mot_b,val);
    digitalWrite(pino2_mot_b,0);
  }
  else {  // sentido de -z
    digitalWrite(pino1_mot_b,0);
    analogWrite(pino2_mot_b,val);
  }
}

void setup(){
  // Inicialização dos pinos do Arduino    
  pinMode(pino1_mot_a,OUTPUT);
  pinMode(pino2_mot_a,OUTPUT);
  pinMode(pino1_mot_b,OUTPUT);
  pinMode(pino2_mot_b,OUTPUT);
  pinMode(pin_led,OUTPUT);
  Serial.begin(115200);

}

void loop(){
  if(count == 255){
    count = 0;
  }

  delay(50);
  count++;
  atua_motor_a(count);
  atua_motor_b(count);
}
