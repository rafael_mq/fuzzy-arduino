// Programa de identificação do theta de equilibrio
/*
 * 
 *  
*/
float theta, x, y, z;
const float maximo = 1.37975, minimo = 1.278511;
const int pino_x = A0,
          pino_y = A1,
          pino_z = A2,
          T = 10,   //ms
          led = 13,
          n = 30;
int t_inicial, t_atual;

void setup() {
  Serial.begin(57600);   //57600, 38400 19200
  pinMode(led,OUTPUT);
  t_inicial = millis();
}

void loop() {
  // amostrando
  t_atual = millis();
  if(t_atual-t_inicial>=T){  
    x = -0.06146*analogRead(pino_x) + 20.41; 
    z = -0.06277*analogRead(pino_z) + 16.35;
    theta = atan(x/z);
    if(theta < maximo && theta >minimo)
      digitalWrite(led,1);
    else
      digitalWrite(led,0);
    Serial.print(x,6);
    Serial.print("\t");
    Serial.print(z,6);
    Serial.print("\t");
    Serial.println(theta,6);
    t_inicial = millis();
  }
}
