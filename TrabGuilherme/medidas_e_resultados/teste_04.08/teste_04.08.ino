#define fp(x) (x==0? 0 : x*(255-40)/100 + 40)
#define fn(x) (x*(255-40)/100 - 40)     // zona morta de 40
#define mf(x) (x>=0? fp(x):fn(x))
//////////////////////////////////////////////////////////////////////////////
char b;
const int pin_led = 13,
          pino1_mot_a = 5,
          pino2_mot_a = 6,
          pino1_mot_b = 9,
          pino2_mot_b = 10;
float e_w_a[4] = {0,0,0,0},
      e_w_b[4] = {0,0,0,0};
float ei_w_a = 0, ei_w_b = 0;
float De_w_a, De_w_b;
float Kp_w = 1,
      Kd_w = 0,
      Ki_w = 0;
float m= 0,m_w = 0,m_s = 0 , w_a, wm_a, w_b,w[3]={0,0,0};
const float M_MAX = 100;
const float M_MIN = -100;
float teste[12] = {8,-8, 6, -6, 4, -4, 3, -3, 2, -2, 1, -1};
int printCounter=0, i;
unsigned long count=0, inicio;

///////////////// ROTINAS DAS INTERRUPÇÕES DOS ENCODERS ///////////
volatile unsigned long t0_a=0 , t0_b=0, t,t_a = 0,t_b = 0;
volatile byte pin_a0=0;
volatile byte pin_b0=0;
volatile byte pin_a1=0;
volatile byte pin_b1=0;
volatile long pulsos_a=0,pulsos_b=0;
long p0a=0,p0b=0;
unsigned long t_min = 1;


void encoder_a(){
  
  pin_a0=pin_a1;
  pin_a1=digitalRead(2);
  if((pin_a0==0)&&(pin_a1==1))
    t0_a=t;
  else
  if((pin_a0==1)&&(pin_a1==0))
  {
    if((t-t0_a)>t_min)
    {
      pulsos_a++;
    }
  }
}

void encoder_b(){
  pin_b0=pin_b1;
  pin_b1=digitalRead(3);
  if((pin_b0==0)&&(pin_b1==1))
    t0_b=t;
  else
  if((pin_b0==1)&&(pin_b1==0))
  {
    if((t-t0_b)>t_min)
    {
      pulsos_b++;
    }
  }
}

inline void atua_motor_a(int value){//recebe um valor de -255 a 255
  byte dir=0;
  unsigned char val = value;
  if (value<0){ 
    dir=1; 
    value=-value;
  }
  
  if(value>255)
    val=255;
  else
    val=value;
  
  if(dir==0){    // sentido de z
    analogWrite(pino1_mot_a,val);
    digitalWrite(pino2_mot_a,0);
  }
  else {  // sentido de -z
    digitalWrite(pino1_mot_a,0);
    analogWrite(pino2_mot_a,val);
  }
}

inline void atua_motor_b(int value){//recebe um valor de -255 a 255
  byte dir=0;
  unsigned char val = value;
  if (value<0){ 
    dir=1; 
    value=-value;
  }
  
  if(value>255)
    val=255;
  else
    val=value;
  
  if(dir==0){    // sentido de z
    analogWrite(pino1_mot_b,val);
    digitalWrite(pino2_mot_b,0);
  }
  else {  // sentido de -z
    digitalWrite(pino1_mot_b,0);
    analogWrite(pino2_mot_b,val);
  }
}



void setup() {
  pinMode(pino1_mot_a,OUTPUT);
  pinMode(pino2_mot_a,OUTPUT);
  pinMode(pino1_mot_b,OUTPUT);
  pinMode(pino2_mot_b,OUTPUT);
  pinMode(pin_led,OUTPUT);
  Serial.begin(115200);
  attachInterrupt(0, encoder_a, RISING);  //pino 2
  attachInterrupt(1, encoder_b, RISING);  //pino 3
}

void loop() {
  /*if(Serial.available()>0){
   b = Serial.read();
   if( b=='P')
      Kp_w = Serial.parseFloat();
   if( b=='I')
      Ki_w = Serial.parseFloat();
   if( b=='D')
      Kd_w = Serial.parseFloat();
   if( b == 'm')
      m = Serial.parseFloat();
  }*/
  for(i=0 ; i<12 ;i++){
    inicio = millis();
    m = teste[i];
    atua_motor_a(mf(m));
    t=inicio;
    while(t-inicio<2000){
      t = millis();
              if(pulsos_a>p0a){
                  w_a = 1000.0/(t - t_a);
                  t_a = t;
                  p0a = pulsos_a;     
              }
              if(t-t_a>100)
                w_a = 0;
              if((t-inicio)%5==0){
                Serial.print(t);
                Serial.print("\t");
                Serial.print(m);
                Serial.print("\t");
                Serial.println(w_a);
              }
    }
  }
}
