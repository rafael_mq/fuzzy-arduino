#define fp(x) (x*(255-10)/100 + 10)
#define fn(x) (x*(255-10)/100 - 10)
#define mf(x) (x>=0? fp(x):fn(x))

float x, z, y;
char b;
const int pino_x = A0,
          pino_y = A1,
          pino_z = A2,
          led = 13,
          pino1_mot_a = 3,
          pino2_mot_a = 9,
          pino1_mot_b = 10,
          pino2_mot_b = 11;
float PID_PV;
float e[4]= {0,0,0,0};
float ei = 0;
float De;
float Kp = 600;
float Kd = 0;
float Ki = 0;
float PID_SPoff=0;
float PID_SP;
float m = 0;
const float M_MAX = 100;
const float M_MIN = -100;
int teste = -50, i;
void atua_motor(int val){//recebe um valor de -255 a 255
  if(val>=0){    // sentido de z
    analogWrite(pino1_mot_a,val);
    digitalWrite(pino2_mot_a,0);
    analogWrite(pino1_mot_b,val);
    digitalWrite(pino2_mot_b,0);
  }
  else {  // sentido de -z
    val = -val;
    digitalWrite(pino1_mot_a,0);
    analogWrite(pino2_mot_a,val);
    digitalWrite(pino1_mot_b,0);
    analogWrite(pino2_mot_b,val);
  }
}
void setup() {
  pinMode(pino1_mot_a,OUTPUT);
  pinMode(pino2_mot_a,OUTPUT);
  pinMode(pino1_mot_b,OUTPUT);
  pinMode(pino2_mot_b,OUTPUT);
  pinMode(led,OUTPUT);
  Serial.begin(57600);
  PID_SP = -0.29;        //provavelmente muda com os motores
}

void loop() {
  if(Serial.available()>0){
   b = Serial.read();
   if( b=='P')
      Kp = Serial.parseFloat();
   if( b=='I')
      Ki = Serial.parseFloat();
   if( b=='D')
      Kd = Serial.parseFloat();
    if( b == 'S')
      PID_SP = Serial.parseFloat();
  }
  delay(10);
  y = -0.05972*analogRead(pino_y) + 21.36; 
  z = -0.06277*analogRead(pino_z) + 16.35;
  PID_PV = atan(z/y);

  if(PID_PV < 1.1*PID_SP && PID_PV >0.9*PID_SP)
      digitalWrite(led,1);
   else
      digitalWrite(led,0);
  for(i = 3;i>0;i--)    //erro sendo atualizado
    e[i] = e[i-1];
  
  e[0] = PID_PV - PID_SP + PID_SPoff ;
  
  De = (e[0] + 3*e[1] - 3*e[2] - e[3] )/6;
  ei+=e[0];

  m=Kp*e[0]+Kd*De+Ki*ei;
  Serial.print(e[0]);
  Serial.print("\t");
  Serial.println(m);
  if (m>M_MAX) {    // anti wind-up
    ei -= e[0];
    m=M_MAX;
  }
  if (m<M_MIN) {
    ei -= e[0];
    m=M_MIN;      
  }
  atua_motor(mf(m));
}
