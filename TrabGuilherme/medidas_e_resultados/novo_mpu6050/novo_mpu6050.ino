#define fp(x) (x*(255-40)/100 + 40)
#define fn(x) (x*(255-40)/100 - 40)
#define mf(x) (x>=0? fp(x):fn(x))
//////////////////////////////////////////////////////////////////////////////
#include "I2Cdev.h"
#include "MPU6050_6Axis_MotionApps20.h"

MPU6050 mpu;

// MPU control/status vars
bool dmpReady = false;  // set true if DMP init was successful
uint8_t mpuIntStatus;   // holds actual interrupt status byte from MPU
uint8_t devStatus;      // return status after each device operation (0 = success, !0 = error)
uint16_t packetSize;    // expected DMP packet size (default is 42 bytes)
uint16_t fifoCount;     // count of all bytes currently in FIFO
uint8_t fifoBuffer[64]; // FIFO storage buffer

// orientation/motion vars
Quaternion q;           // [w, x, y, z]         quaternion container
VectorFloat gravity;    // [x, y, z]            gravity vector
float ypr[3];           // [yaw, pitch, roll]   yaw/pitch/roll container and gravity vector
/////////////////////////////////////////////////////////////////////////////////////////////////
char b;
const int pin_led = 13,
          pino1_mot_a = 5,
          pino2_mot_a = 6,
          pino1_mot_b = 9,
          pino2_mot_b = 10;

float PV[3]={0,0,0},faixa = 15;
float PID_PV;
float e[4] = {0,0,0,0},
      e_w_a[4] = {0,0,0,0},
      e_w_b[4] = {0,0,0,0};
float ei, ei_w_a, ei_w_b;
float De, De_w_a, De_w_b;
#define ACT (-1)
float Kp = 120, //600;
      Kd = 0, //4;
      Ki = 0, //8;
      Kp_w = 1.2,
      Kd_w = 0.1,
      Ki_w = 0.08;
float PID_SPoff=0;
float PID_SP=-0.42;
float m_theta = 0,m_w = 0,m_s = 0 , w_a=0, w_b=0,m=0;
const float M_MAX = 100;
const float M_MIN = -100;
int teste = -50;
byte printCounter=0;
byte actCounter=0;
///////////////// ROTINAS DAS INTERRUPÇÕES DOS ENCODERS ///////////
volatile unsigned long t0_a=0 , t0_b=0, t,t_a = 0,t_b = 0;
volatile byte pin_a0=0;
volatile byte pin_b0=0;
volatile byte pin_a1=0;
volatile byte pin_b1=0;
volatile long pulsos_a=0,pulsos_b=0;
long p0a=0,p0b=0;
unsigned long t_min = 1;


void encoder_a(){
  
  pin_a0=pin_a1;
  pin_a1=digitalRead(2);
  if((pin_a0==0)&&(pin_a1==1))
    t0_a=t;
  else
  if((pin_a0==1)&&(pin_a1==0))
  {
    if((t-t0_a)>t_min)
    {
      pulsos_a++;
    }
  }
}

void encoder_b(){
  pin_b0=pin_b1;
  pin_b1=digitalRead(3);
  if((pin_b0==0)&&(pin_b1==1))
    t0_b=t;
  else
  if((pin_b0==1)&&(pin_b1==0))
  {
    if((t-t0_b)>t_min)
    {
      pulsos_b++;
    }
  }
}


inline void atua_motor_a(int value){//recebe um valor de -255 a 255
  byte dir=0;
  unsigned char val = value;
  if (value<0){ 
    dir=1; 
    value=-value;
  }
  
  if(value>255)
    val=255;
  else
    val=value;
  
  if(dir==0){    // sentido de z
    analogWrite(pino1_mot_a,val);
    digitalWrite(pino2_mot_a,0);
  }
  else {  // sentido de -z
    digitalWrite(pino1_mot_a,0);
    analogWrite(pino2_mot_a,val);
  }

  if(value > 0){
  	
  }
}

inline void atua_motor_b(int value){//recebe um valor de -255 a 255
  byte dir=0;
  unsigned char val = value;
  if (value<0){ 
    dir=1; 
    value=-value;
  }
  
  if(value>255)
    val=255;
  else
    val=value;
  
  if(dir==0){    // sentido de z
    analogWrite(pino1_mot_b,val);
    digitalWrite(pino2_mot_b,0);
  }
  else {  // sentido de -z
    digitalWrite(pino1_mot_b,0);
    analogWrite(pino2_mot_b,val);
  }
}



void setup() {
	pinMode(pino1_mot_a,OUTPUT);
	pinMode(pino2_mot_a,OUTPUT);
	pinMode(pino1_mot_b,OUTPUT);
	pinMode(pino2_mot_b,OUTPUT);
	pinMode(pin_led,OUTPUT);
	Serial.begin(115200);
	ei=0;
	attachInterrupt(0, encoder_a, RISING);  //pino 2
	attachInterrupt(1, encoder_b, RISING);  //pino 3

	Serial.println(F("Initializing DMP..."));
	devStatus = mpu.dmpInitialize();

	// supply your own gyro offsets here, scaled for min sensitivity
	mpu.setXGyroOffset(220);
	mpu.setYGyroOffset(76);
	mpu.setZGyroOffset(-85);
	mpu.setZAccelOffset(1788); // 1688 factory default for my test chip

	// make sure it worked (returns 0 if so)
	if (devStatus == 0) 
	{
	// turn on the DMP, now that it's ready
		Serial.println(F("Enabling DMP..."));
		mpu.setDMPEnabled(true);

		// enable Arduino interrupt detection
		Serial.println(F("Enabling interrupt detection (Arduino external interrupt 0)..."));

		// set our DMP Ready flag so the main loop() function knows it's okay to use it
		Serial.println(F("DMP ready! Waiting for first interrupt..."));
		dmpReady = true;

		// get expected DMP packet size for later comparison
		packetSize = mpu.dmpGetFIFOPacketSize();
	} else {
		// ERROR!
		// 1 = initial memory load failed
		// 2 = DMP configuration updates failed
		// (if it's going to break, usually the code will be 1)
		Serial.print(F("DMP Initialization failed (code "));
		Serial.print(devStatus);
		Serial.println(F(")"));
	}
	PID_SP = 0.43;//0.3304;
}





void loop() {
	
  if(Serial.available()>0){
   b = Serial.read();
    if( b=='P')
      Kp = Serial.parseFloat();
   if( b=='I')
      Ki = Serial.parseFloat();
   if( b=='D')
      Kd = Serial.parseFloat();
    if( b == 'S')
      PID_SP = Serial.parseFloat();
  }
    ///////////////////////////////////////////////////////////////////////////////////////
    // get current FIFO count
    fifoCount = mpu.getFIFOCount();

    // check for overflow (this should never happen unless our code is too inefficient)
    if (fifoCount == 1024) {
        // reset so we can continue cleanly
        mpu.resetFIFO();
        Serial.println(F("FIFO overflow!"));

    // otherwise, check for DMP data ready interrupt (this should happen frequently)
    } else{
        // wait for correct available data length, should be a VERY short wait
        while (fifoCount < packetSize) fifoCount = mpu.getFIFOCount();

        // read a packet from FIFO
        mpu.getFIFOBytes(fifoBuffer, packetSize);
        
        // track FIFO count here in case there is > 1 packet available
        // (this lets us immediately read more without waiting for an interrupt)
        fifoCount -= packetSize;
            mpu.dmpGetQuaternion(&q, fifoBuffer);
            mpu.dmpGetGravity(&gravity, &q);
            mpu.dmpGetYawPitchRoll(ypr, &q, &gravity);
            
            PID_PV = ypr[1];  //pitch em radianos
    }
  if(PID_PV < (1.1*PID_SP) && PID_PV > (0.9*PID_SP))
      digitalWrite(pin_led,1);
  else
      digitalWrite(pin_led,0);
  ///////////////// MALHA DO ANGULO ////////////////////////////////////
  e[3]=e[2];
  e[2]=e[1];
  e[1]=e[0];
  
  e[0] = ACT*(PID_PV - PID_SP) + PID_SPoff ;
  
  De = (e[0] + 3*e[1] - 3*e[2] - e[3] )/6;
  ei+=e[0];

  m = Kp*e[0] + Kd*De + Ki*ei;    
  //////////////////////////////////////////////////////////////
  t = millis();
  if(pulsos_a>p0a){
              if((t-t_a)!=0)  // overflow
                w_a = 1000.0/(t - t_a);
              
              t_a = t;
              
              p0a = pulsos_a;
              ///////////////// MALHA DA ROTACAO DO MOTOR A ////////////////////////////////////
                e_w_a[3]=e_w_a[2];
                e_w_a[2]=e_w_a[1];
                e_w_a[1]=e_w_a[0];
                
                e_w_a[0] = abs(m)-w_a ; 
                De_w_a = (e_w_a[0] + 3*e_w_a[1] - 3*e_w_a[2] - e_w_a[3] )/6;
                ei_w_a+=e_w_a[0];
                
                m_w = Kp_w*e_w_a[0] + Kd_w*De_w_a + Ki_w*ei_w_a;
                
                if(m>0)
                  m_w = m_w>=0 ? m_w:0;
                else if(m<0)
                  m_w = m_w>=0 ? -m_w:0;
                else
                  m_w = 0;
                
                // corrigindo saturação e wind-up
                
                if (M_MAX<m_w) {
                    ei_w_a-=e_w_a[0];
                    m_w=M_MAX;
                 }
                if (M_MIN>m_w) {
                    ei_w_a-=e_w_a[0];
                    m_w=M_MIN;      
                  }
                if(abs(m_w)<faixa)
                    atua_motor_a(0);      
                  else
                    atua_motor_a(mf(m_w));
                
          }
          if(pulsos_b>p0b){
              if((t-t_b)!=0)  // overflow
                w_b = 1000.0/(t - t_b);
              
              t_b = t;
              
              p0b = pulsos_b;
              ///////////////// MALHA DA ROTACAO DO MOTOR B ////////////////////////////////////
                e_w_b[3]=e_w_b[2];
                e_w_b[2]=e_w_b[1];
                e_w_b[1]=e_w_b[0];
                
                e_w_b[0] = abs(m)-w_b ;
                De_w_b = (e_w_b[0] + 3*e_w_b[1] - 3*e_w_b[2] - e_w_b[3] )/6;
                ei_w_b+=e_w_b[0];
                
                m_w = Kp_w*e_w_b[0] + Kd_w*De_w_b + Ki_w*ei_w_b;
                
                if(m>0)
                  m_w = m_w>=0 ? m_w:0;
                else if(m<0)
                  m_w = m_w>=0 ? -m_w:0;
                else
                  m_w = 0;
                
                // corrigindo saturação e wind-up
                
                if (M_MAX<m_w) {
                    ei_w_b-=e_w_b[0];
                    m_w=M_MAX;
                 }
                if (M_MIN>m_w) {
                    ei_w_b-=e_w_b[0];
                    m_w=M_MIN;      
                  }
                if(abs(m_w)<faixa)
                    atua_motor_b(0);      
                  else
                    atua_motor_b(mf(m_w));
                
          }
          if(t-t_a>100){
            w_a = 0;
              ///////////////// MALHA DA ROTACAO DO MOTOR A ////////////////////////////////////
                /*w[2] = w[1];
                w[1]= w[0];
                w[0] = w_a;
                wm_a = (w[0] + w[1] + w[2])/3;*/
                e_w_a[3]=e_w_a[2];
                e_w_a[2]=e_w_a[1];
                e_w_a[1]=e_w_a[0];
                
                e_w_a[0] = abs(m)-w_a ;
                
                De_w_a = (e_w_a[0] + 3*e_w_a[1] - 3*e_w_a[2] - e_w_a[3] )/6;
                ei_w_a+=e_w_a[0];
                
                m_w = Kp_w*e_w_a[0] + Kd_w*De_w_a + Ki_w*ei_w_a;
                
                if(m>0)
                  m_w = m_w>=0 ? m_w:0;
                else if(m<0)
                  m_w = m_w>=0 ? -m_w:0;
                else
                  m_w = 0;
                
                
                // corrigindo saturação e wind-up
                
                if (M_MAX<m_w) {
                    ei_w_a-=e_w_a[0];
                    m_w=M_MAX;
                 }
                if (M_MIN>m_w) {
                    ei_w_a-=e_w_a[0];
                    m_w=M_MIN;      
                  }
                if(abs(m_w)<faixa)
                    atua_motor_a(0);      
                  else
                    atua_motor_a(mf(m_w));
                
          }
          
          if(t-t_b>100){
              w_b = 0;
              ///////////////// MALHA DA ROTACAO DO MOTOR B ////////////////////////////////////
                e_w_b[3]=e_w_b[2];
                e_w_b[2]=e_w_b[1];
                e_w_b[1]=e_w_b[0];
                
                e_w_b[0] = abs(m)-w_b ;
                De_w_b = (e_w_b[0] + 3*e_w_b[1] - 3*e_w_b[2] - e_w_b[3] )/6;
                ei_w_b+=e_w_b[0];
                
                m_w = Kp_w*e_w_b[0] + Kd_w*De_w_b + Ki_w*ei_w_b;
                
                if(m>0)
                  m_w = m_w>=0 ? m_w:0;
                else if(m<0)
                  m_w = m_w>=0 ? -m_w:0;
                else
                  m_w = 0;
                
                // corrigindo saturação e wind-up
                
                if (M_MAX<m_w) {
                    ei_w_b-=e_w_b[0];
                    m_w=M_MAX;
                 }
                if (M_MIN>m_w) {
                    ei_w_b-=e_w_b[0];
                    m_w=M_MIN;      
                  }
                if(abs(m_w)<faixa)
                    atua_motor_b(0);      
                  else
                    atua_motor_b(mf(m_w));
          }
  
  if(printCounter==80) {
    Serial.print(PID_PV,5);
    Serial.print("\t");
    Serial.print(e[0]);
    Serial.print("\t");
    Serial.println(mf(-m_theta));
    printCounter=0;
  }
  else
    printCounter++;
}
