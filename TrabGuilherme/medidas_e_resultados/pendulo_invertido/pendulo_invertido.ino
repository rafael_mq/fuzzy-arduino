#define fp(x) (x*(255-0)/100 + 0)
#define fn(x) (x*(255-0)/100 - 0)
#define mf(x) (x>=0? fp(x):fn(x))

float x,y, z;
char b;
const float maximo = 0.45, minimo = 0.42;
const int pino_x = A0,
          pino_y = A1,
          pino_z = A2,
          pin_led = 13,
          pino1_mot_a = 5,
          pino2_mot_a = 6,
          pino1_mot_b = 9,
          pino2_mot_b = 10;

float PV[3]={0,0,0};
float PID_PV;
float e[4] = {0,0,0,0};
float ei;
float De;
#define ACT (-1)
float Kp = 90; //600;
float Kd = 0; //4;
float Ki = 0; //8;
float PID_SPoff=0;
float PID_SP=-0.42;
float m = 0;
const float M_MAX = 100;
const float M_MIN = -100;
int teste = -50;
byte printCounter=0;
byte actCounter=0;

inline void atua_motor(int value){//recebe um valor de -255 a 255
  byte dir=0;
  unsigned char val = value;
  if (value<0){ 
    dir=1; 
    value=-value;
  }
  
  if(value>255)
    val=255;
  else
    val=value;
  
  if(dir==0){    // sentido de z
    analogWrite(pino1_mot_a,val);
    //digitalWrite(pino1_mot_a,1);
    
    digitalWrite(pino2_mot_a,0);
    
    analogWrite(pino1_mot_b,val);
    //digitalWrite(pino1_mot_b,1);
    
    digitalWrite(pino2_mot_b,0);
  }
  else {  // sentido de -z
    digitalWrite(pino1_mot_a,0);
    
    analogWrite(pino2_mot_a,val);
    //digitalWrite(pino2_mot_a,1);
    
    digitalWrite(pino1_mot_b,0);
    
    analogWrite(pino2_mot_b,val);
    //digitalWrite(pino2_mot_b,1);
  }
}
void setup() {
  pinMode(pino1_mot_a,OUTPUT);
  pinMode(pino2_mot_a,OUTPUT);
  pinMode(pino1_mot_b,OUTPUT);
  pinMode(pino2_mot_b,OUTPUT);
  pinMode(pin_led,OUTPUT);
  
  Serial.begin(115200);
  ei=0;
  //PID_SP = 0.43;//0.3304;
}

void loop() {
  
  
  if(Serial.available()>0){
   b = Serial.read();
    if( b=='P')
      Kp = Serial.parseFloat();
   if( b=='I')
      Ki = Serial.parseFloat();
   if( b=='D')
      Kd = Serial.parseFloat();
    if( b == 'S')
      PID_SP = Serial.parseFloat();
  }
//  delay(10);
  y = -0.05972*analogRead(pino_y) + 21.36; 
  //x = -0.06146*analogRead(pino_x) + 20.41; 
  z = -0.06277*analogRead(pino_z) + 16.35;
  //PV[2] = PV[1];
  //PV[1] = PV[0];
  PID_PV = atan(z/y);
  //PID_PV = (PV[0]+PV[1]+PV[2])/3;
  
if(PID_PV < (1.1*PID_SP) && PID_PV > (0.9*PID_SP))
      digitalWrite(pin_led,1);
    else
      digitalWrite(pin_led,0);
  
  e[3]=e[2];
  e[2]=e[1];
  e[1]=e[0];
  
  e[0] = ACT*(PID_PV - PID_SP) + PID_SPoff ;
  
  De = (e[0] + 3*e[1] - 3*e[2] - e[3] )/6;
  ei+=e[0];

  m=Kp*e[0]+Kd*De+Ki*ei;
  
  if(PID_PV>(PID_SP+0.1))
    m=2*m; //(-)
  else
  if(PID_PV<(PID_SP-0.1))
    m=2*m; //(+)
  
  
  if(printCounter==80) {
//    Serial.print(y);
//    Serial.print("\t");
//    Serial.print(z);
//    Serial.print("\t");
    Serial.print(PID_PV,5);
    Serial.print("\t");
    Serial.print(e[0]);
    Serial.print("\t");
    Serial.println(mf(-m));
    printCounter=0;
  }
  else
    printCounter++;

  if(actCounter==15) {
    if (M_MAX<m) {
      ei-=e[0];
      m=M_MAX;
    }
    if (M_MIN>m) {
      ei-=e[0];
      m=M_MIN;      
    }
    atua_motor(mf(-m));
    actCounter=0;
  }
  else
    actCounter++;
}
