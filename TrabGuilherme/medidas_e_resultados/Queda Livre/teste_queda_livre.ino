// Queda livre
// Programa de identificação do acelerometro pololu mma 7361LC
/*
 *  O programa abaixo tomará n medidas separadas por T segundos
 *  e as enviará por com. serial, até o momento em que a leitura
 *  analógica atinja um valor equivalente a g ou -g(significa q a queda livre acabou)
*/
const int pino_x = A0,
          pino_y = A1,
          pino_z = A2,
          pino_eletroima = 6,
          T = 10,
          led = 13,
          n = 30,
          duracao = 200;
int amostras = 0, x, y, z, t_inicial, t_atual, i, T0, teste = 0;

void setup() {
  Serial.begin(57600);   //57600, 38400 19200
  pinMode(led,OUTPUT);
  pinMode(pino_eletroima,OUTPUT);
  digitalWrite(pino_eletroima,HIGH);
  for(i=0;i<10;i++){
    Serial.print("Prepare-se : ");
    Serial.println(5-i);
    delay(100*T);
  }
  t_inicial = millis();
  digitalWrite(pino_eletroima,LOW);
  Serial.println("teste 0");
}

void loop() {
  // amostrando
  t_atual = millis();
  if(t_atual-t_inicial>=T){
    x = analogRead(pino_x);
    y = analogRead(pino_y);
    z = analogRead(pino_z);
    amostras++;
    Serial.print(amostras);
    Serial.print("\t");
    Serial.print(x);
    Serial.print("\t");
    Serial.print(y);
    Serial.print("\t");
    Serial.println(z);
    t_inicial = millis();
  }
  if(z > 350 && amostras !=0){    // essa condição muda para cada eixo, basta consultar os valores médios de cada um
    teste++;
    Serial.print("teste");
    Serial.println(teste);
    amostras = 0;
    digitalWrite(pino_eletroima,HIGH);
    delay(20000);                 // tempo usado para "ajeitar" o suporte do sensor
    digitalWrite(pino_eletroima,LOW);
    t_inicial = millis();
  }
 }
