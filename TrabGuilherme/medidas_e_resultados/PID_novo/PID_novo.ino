#define fp(x) (x==0? 0 : x*(255-40)/100 + 40)
#define fn(x) (x*(255-40)/100 - 40)     // zona morta de 40
#define mf(x) (x>=0? fp(x):fn(x))
//////////////////////////////////////////////////////////////////////////////
char b;
const int pin_led = 13,
          pino1_mot_a = 5,
          pino2_mot_a = 6,
          pino1_mot_b = 9,
          pino2_mot_b = 10;
float e_w_a[4] = {0,0,0,0},
      e_w_b[4] = {0,0,0,0};
float ei_w_a = 0, ei_w_b = 0;
float De_w_a, De_w_b;
float Kp_w = 1.2,   //1.2
      Kd_w = 0.1,   //0.1
      Ki_w = 0.08;   //0.03
float m= 0,m_w = 0, w_a = 0, w_b;
const float M_MAX = 100;
const float M_MIN = -100;
int printCounter=0;
unsigned long count=0;
///////////////// ROTINAS DAS INTERRUPÇÕES DOS ENCODERS ///////////
volatile unsigned long t0_a=0 , t0_b=0, t,t_a = 0,t_b = 0;
volatile byte pin_a0=0;
volatile byte pin_b0=0;
volatile byte pin_a1=0;
volatile byte pin_b1=0;
volatile long pulsos_a=0,pulsos_b=0;
long p0a=0,p0b=0;
unsigned long t_min = 1;


void encoder_a(){
  
  pin_a0=pin_a1;
  pin_a1=digitalRead(2);
  if((pin_a0==0)&&(pin_a1==1))
    t0_a=t;
  else
  if((pin_a0==1)&&(pin_a1==0))
  {
    if((t-t0_a)>t_min)
    {
      pulsos_a++;
    }
  }
}

void encoder_b(){
  pin_b0=pin_b1;
  pin_b1=digitalRead(3);
  if((pin_b0==0)&&(pin_b1==1))
    t0_b=t;
  else
  if((pin_b0==1)&&(pin_b1==0))
  {
    if((t-t0_b)>t_min)
    {
      pulsos_b++;
    }
  }
}

inline void atua_motor_a(int value){//recebe um valor de -255 a 255
  byte dir=0;
  unsigned char val = value;
  if (value<0){ 
    dir=1; 
    value=-value;
  }
  
  if(value>255)
    val=255;
  else
    val=value;
  
  if(dir==0){    // sentido de z
    analogWrite(pino1_mot_a,val);
    digitalWrite(pino2_mot_a,0);
  }
  else {  // sentido de -z
    digitalWrite(pino1_mot_a,0);
    analogWrite(pino2_mot_a,val);
  }
}

inline void atua_motor_b(int value){//recebe um valor de -255 a 255
  byte dir=0;
  unsigned char val = value;
  if (value<0){ 
    dir=1; 
    value=-value;
  }
  
  if(value>255)
    val=255;
  else
    val=value;
  
  if(dir==0){    // sentido de z
    analogWrite(pino1_mot_b,val);
    digitalWrite(pino2_mot_b,0);
  }
  else {  // sentido de -z
    digitalWrite(pino1_mot_b,0);
    analogWrite(pino2_mot_b,val);
  }
}



void setup() {
  pinMode(pino1_mot_a,OUTPUT);
  pinMode(pino2_mot_a,OUTPUT);
  pinMode(pino1_mot_b,OUTPUT);
  pinMode(pino2_mot_b,OUTPUT);
  pinMode(pin_led,OUTPUT);
  Serial.begin(115200);
  attachInterrupt(0, encoder_a, CHANGE);  //pino 2
  attachInterrupt(1, encoder_b, CHANGE);  //pino 3
}

void loop() {
  if(Serial.available()>0){
   b = Serial.read();
   if( b=='P')
      Kp_w = Serial.parseFloat();
   if( b=='I')
      Ki_w = Serial.parseFloat();
   if( b=='D')
      Kd_w = Serial.parseFloat();
   if( b == 'm')
      m = Serial.parseFloat();
  }
  t = millis();
          if(pulsos_a>p0a){
              w_ant = w_a;
              if((t-t_a)!=0)  // overflow
                w_a = 1000.0/(t - t_a);
              
              t_a = t;
              
              p0a = pulsos_a;
              ///////////////// MALHA DA ROTACAO DO MOTOR A ////////////////////////////////////
                e_w_a[3]=e_w_a[2];
                e_w_a[2]=e_w_a[1];
                e_w_a[1]=e_w_a[0];
                
                e_w_a[0] = abs(m)-w_a ; 
                De_w_a = (e_w_a[0] + 3*e_w_a[1] - 3*e_w_a[2] - e_w_a[3] )/6;
                ei_w_a+=e_w_a[0];
                
                m_w = Kp_w*e_w_a[0] + Kd_w*De_w_a + Ki_w*ei_w_a;
                
                if(m>0)
                  m_w = m_w>=0 ? m_w:0;
                else if(m<0)
                  m_w = m_w>=0 ? -m_w:0;
                else
                  m_w = 0;
                
                // corrigindo saturação e wind-up
                
                if (M_MAX<m_w) {
                    ei_w_a-=e_w_a[0];
                    m_w=M_MAX;
                 }
                if (M_MIN>m_w) {
                    ei_w_a-=e_w_a[0];
                    m_w=M_MIN;      
                  }
                if(abs(m_w)<3)
                    atua_motor_a(0);      
                  else
                    atua_motor_a(mf(m_w));
                
          }
          if(pulsos_b>p0b){
              if((t-t_b)!=0)  // overflow
                w_b = 1000.0/(t - t_b);
              
              t_b = t;
              
              p0b = pulsos_b;
              ///////////////// MALHA DA ROTACAO DO MOTOR B ////////////////////////////////////
                e_w_b[3]=e_w_b[2];
                e_w_b[2]=e_w_b[1];
                e_w_b[1]=e_w_b[0];
                
                e_w_b[0] = abs(m)-w_b ;
                De_w_b = (e_w_b[0] + 3*e_w_b[1] - 3*e_w_b[2] - e_w_b[3] )/6;
                ei_w_b+=e_w_b[0];
                
                m_w = Kp_w*e_w_b[0] + Kd_w*De_w_b + Ki_w*ei_w_b;
                
                if(m>0)
                  m_w = m_w>=0 ? m_w:0;
                else if(m<0)
                  m_w = m_w>=0 ? -m_w:0;
                else
                  m_w = 0;
                
                // corrigindo saturação e wind-up
                
                if (M_MAX<m_w) {
                    ei_w_b-=e_w_b[0];
                    m_w=M_MAX;
                 }
                if (M_MIN>m_w) {
                    ei_w_b-=e_w_b[0];
                    m_w=M_MIN;      
                  }
                if(abs(m_w)<3)
                    atua_motor_b(0);      
                  else
                    atua_motor_b(mf(m_w));
                
          }
          if(t-t_a>100){
            w_a = 0;
              ///////////////// MALHA DA ROTACAO DO MOTOR A ////////////////////////////////////
                /*w[2] = w[1];
                w[1]= w[0];
                w[0] = w_a;
                wm_a = (w[0] + w[1] + w[2])/3;*/
                e_w_a[3]=e_w_a[2];
                e_w_a[2]=e_w_a[1];
                e_w_a[1]=e_w_a[0];
                
                e_w_a[0] = abs(m)-w_a ;
                
                De_w_a = (e_w_a[0] + 3*e_w_a[1] - 3*e_w_a[2] - e_w_a[3] )/6;
                ei_w_a+=e_w_a[0];
                m_want = m_w;
                
                m_w = Kp_w*e_w_a[0] + Kd_w*De_w_a + Ki_w*ei_w_a;
                
                if(m>0)
                  m_w = m_w>=0 ? m_w:0;
                else if(m<0)
                  m_w = m_w>=0 ? -m_w:0;
                else
                  m_w = 0;
                
                
                // corrigindo saturação e wind-up
                
                if (M_MAX<m_w) {
                    ei_w_a-=e_w_a[0];
                    m_w=M_MAX;
                 }
                if (M_MIN>m_w) {
                    ei_w_a-=e_w_a[0];
                    m_w=M_MIN;      
                  }
                if(abs(m_w)<3)
                    atua_motor_a(0);      
                  else
                    atua_motor_a(mf(m_w));
                
          }
          
          if(t-t_b>100){
              w_b = 0;
              ///////////////// MALHA DA ROTACAO DO MOTOR B ////////////////////////////////////
                e_w_b[3]=e_w_b[2];
                e_w_b[2]=e_w_b[1];
                e_w_b[1]=e_w_b[0];
                
                e_w_b[0] = abs(m)-w_b ;
                De_w_b = (e_w_b[0] + 3*e_w_b[1] - 3*e_w_b[2] - e_w_b[3] )/6;
                ei_w_b+=e_w_b[0];
                
                m_w = Kp_w*e_w_b[0] + Kd_w*De_w_b + Ki_w*ei_w_b;
                
                if(m>0)
                  m_w = m_w>=0 ? m_w:0;
                else if(m<0)
                  m_w = m_w>=0 ? -m_w:0;
                else
                  m_w = 0;
                
                // corrigindo saturação e wind-up
                
                if (M_MAX<m_w) {
                    ei_w_b-=e_w_b[0];
                    m_w=M_MAX;
                 }
                if (M_MIN>m_w) {
                    ei_w_b-=e_w_b[0];
                    m_w=M_MIN;      
                  }
                if(abs(m_w)<3)
                    atua_motor_b(0);      
                  else
                    atua_motor_b(mf(m_w));
          }
          if(printCounter==4000){
            Serial.print(m);
            Serial.print("\t");
            /*Serial.print(m_w);
            Serial.print("\t");
            Serial.print(w_a);
            Serial.print("\t");*/
            /*Serial.print(e_w_a[0]);
            Serial.print("\t");
            Serial.println(e_w_b[0]);*/
            Serial.print(w_a);
            Serial.print("\t");
            Serial.println(w_b);
            /*Serial.print("\t");
            Serial.print(w_b);
            Serial.print("\t");
            Serial.println(e_w_b[0]);*/
            printCounter = 0;
          }else
            printCounter++;
}
