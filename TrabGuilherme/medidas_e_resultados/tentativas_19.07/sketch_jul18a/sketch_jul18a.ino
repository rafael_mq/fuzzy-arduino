#define fp(x) (x*(255-25)/100 + 25)
#define fn(x) (x*(255-25)/100 - 25)
#define mf(x) (x>=0? fp(x):fn(x))
/* PID Z&N
 *   K = 0.6*K_crit;
 *   Ti = 0.5*P_crit;
 *   Td = 0.125*P_crit;
 *   m = m1 + q0*e[0] + q1*e[1] + q2*e[2];
 *   q0 = Kp*(1 + Ts/(2*Ti) + Td/(Ts));
 *   q1 = -Kp*(1- Ts/(2*Ti) + 2*Td/Ts);
 *   q2 = Kp*Td/Ts;
 */
float x, y[3] ={0,0,0}, z = 0,Y;
char b;
const int pino_x = A0,
          pino_y = A1,
          pino_z = A2,
          pin_led = 13,
          pino1_mot_a = 3,
          pino2_mot_a = 9,
          pino1_mot_b = 10,
          pino2_mot_b = 11;
float PV[2]={0,0};
float PID_PV;
float e[4] = {0,0,0,0};
float ei,z_ant;
int cont_z = 0;
float De;
#define ACT (-1)
float Kp = 1,
      q0,
      q1,
      q2; //600;
float Kd = 0; //4;
float Ki = 0; //8;
float PID_SPoff= 0;
float PID_SP=-0.29;
float m = 0,m1;
const float M_MAX = 100;
const float M_MIN = -100;
float Ts = 10, t_atual,t_inicial,Ti = 3.4028e38, Td = 0;
byte printCounter=0;
byte actCounter=0;
int i;

inline void atua_motor(int val){//recebe um valor de -255 a 255
  if(val>=0){    // sentido de z
    analogWrite(pino1_mot_a,val);
    digitalWrite(pino2_mot_a,0);
    analogWrite(pino1_mot_b,val);
    digitalWrite(pino2_mot_b,0);
  }
  else {  // sentido de -z
    val = -val;
    digitalWrite(pino1_mot_a,0);
    analogWrite(pino2_mot_a,val);
    digitalWrite(pino1_mot_b,0);
    analogWrite(pino2_mot_b,val);
  }
}
void setup() {
  pinMode(pino1_mot_a,OUTPUT);
  pinMode(pino2_mot_a,OUTPUT);
  pinMode(pino1_mot_b,OUTPUT);
  pinMode(pino2_mot_b,OUTPUT);
  pinMode(pin_led,OUTPUT);
  
  Serial.begin(115200);
  ei=0;
  PID_SP = 1.55;//0.3304;
  t_inicial = millis();
}

void loop() {
  if(Serial.available()>0){
   b = Serial.read();
    if( b=='P')
      Kp = Serial.parseFloat();
   if( b=='I')
      Ti = Serial.parseFloat();
   if( b=='D')
      Td = Serial.parseFloat();
    if( b == 'S')
      PID_SP = Serial.parseFloat();
  } 
      y[0] = -0.05972*analogRead(pino_y) + 21.36;
      //x = -0.06146*analogRead(pino_x) + 20.41;
      z_ant = z;
      z = -0.06277*analogRead(pino_z) + 16.35;
      if((z>0 && z_ant>0)|| (z<0 && z_ant<0)) 
        cont_z++;
      else
        cont_z = 0;
  t_atual = millis();
  
  if(t_atual-t_inicial>=Ts){
      
      y[2] = y[1];
      y[1] = y[0];
      y[0] = -0.05972*analogRead(pino_y) + 21.36;
      if(y[0]>y[1]){
        if(y[0]<y[2])
          Y = y[0];
         else
          Y = y[2];
      }else{
        if(y[1]<y[2])
          Y = y[1];
        else
          Y = y[2];
      }
      PV[1] = PV[0];
      if(Y < -9.917)
        Y = -9.917;
      else if(Y > 9.917)
        Y = 9.917;
      PV[0] = acos(Y/-9.917);
      if(cont_z>=100)
        PV[0] = (PV[0]>0&&z>0)? PV[0] : -PV[0]; // se tiverem o mesmo sinal
      PID_PV = (PV[0]+PV[1])/2;
      
    if(PID_PV < (1.1*PID_SP) && PID_PV > (0.9*PID_SP))
          digitalWrite(pin_led,1);
        else
          digitalWrite(pin_led,0);
      for( i = 3 ; i>0 ; i--)
        e[i] = e[i-1];
      
      //e[0] = ACT*(PID_PV - PID_SP) + PID_SPoff ;
      e[0] = (PID_PV - PID_SP) + PID_SPoff ;
      /*
      q0 = Kp*(1.0 + Ts/(2*Ti) + Td/Ts);
      q1 = -Kp*(1.0 - Ts/(2*Ti) + 2.0*Td/Ts);
      q2 = Kp*Td/Ts;
      */
      De = (e[0] + 3*e[1] - 3*e[2] - e[3] )/6;
      ei+=e[0];
      /*m1 = m;
      m = m1 + q0*e[0] + q1*e[1] + q2*e[2];*/
      m=Kp*e[0]+Kd*De+Ki*ei;
      
      /*
      if(PID_PV>(PID_SP+0.1))
        m=2*m; //(-)
      else
      if(PID_PV<(PID_SP-0.1))
        m=2*m; //(+)
      */
      
      if(printCounter==20) {
    //    Serial.print(y);
    //    Serial.print("\t");
    //    Serial.print(z);
    //    Serial.print("\t");
        Serial.print(PID_PV,3);
        Serial.print("\t");
        Serial.print(e[0]);
        Serial.print("\t");
        Serial.println(mf(m));
        printCounter=0;
      }
      else
        printCounter++;
    
        if (M_MAX<m) {
          ei-=e[0];
          m=M_MAX;
        }
        if (M_MIN>m) {
          ei-=e[0];
          m=M_MIN; 
        }
        if(e[0] > -0.15 && e[0] < 0.15)
          atua_motor(0);
        else
          atua_motor(mf(-m));

       t_inicial = millis();
  }
}
