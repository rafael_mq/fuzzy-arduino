#define fp(x) (x==0? 0 : x*(255-35)/100 + 35)
#define fn(x) (x*(255-35)/100 - 35)     // zona morta de 35
#define mf(x) (x>=0? fp(x):fn(x))
//////////////////////////////////////////////////////////////////////////////
char b;
const int pin_led = 13,
          pino1_mot_a = 5,
          pino2_mot_a = 6,
          pino1_mot_b = 9,
          pino2_mot_b = 10;
float m = 0, w_a,w_b;
unsigned long inicio, fim, count;
///////////////// ROTINAS DAS INTERRUPÇÕES DOS ENCODERS ///////////
volatile unsigned long t0_a=0 , t0_b=0, t,t_a = 0,t_b = 0;
volatile byte pin_a0=0;
volatile byte pin_b0=0;
volatile byte pin_a1=0;
volatile byte pin_b1=0;
volatile long pulsos_a=0,pulsos_b=0;
long p0a=0,p0b=0;
unsigned long t_min = 1;


void encoder_a(){
  
  pin_a0=pin_a1;
  pin_a1=digitalRead(2);
  if((pin_a0==0)&&(pin_a1==1))
    t0_a=t;
  else
  if((pin_a0==1)&&(pin_a1==0))
  {
    if((t-t0_a)>t_min)
    {
      pulsos_a++;
    }
  }
}

void encoder_b(){
  pin_b0=pin_b1;
  pin_b1=digitalRead(3);
  if((pin_b0==0)&&(pin_b1==1))
    t0_b=t;
  else
  if((pin_b0==1)&&(pin_b1==0))
  {
    if((t-t0_b)>t_min)
    {
      pulsos_b++;
    }
  }
   
}

inline void atua_motor_a(int value){//recebe um valor de -255 a 255
  byte dir=0;
  unsigned char val = value;
  if (value<0){ 
    dir=1; 
    value=-value;
  }
  
  if(value>255)
    val=255;
  else
    val=value;
  
  if(dir==0){    // sentido de z
    analogWrite(pino1_mot_a,val);
    digitalWrite(pino2_mot_a,0);
  }
  else {  // sentido de -z
    digitalWrite(pino1_mot_a,0);
    analogWrite(pino2_mot_a,val);
  }
}

inline void atua_motor_b(int value){//recebe um valor de -255 a 255
  byte dir=0;
  unsigned char val = value;
  if (value<0){ 
    dir=1; 
    value=-value;
  }
  
  if(value>255)
    val=255;
  else
    val=value;
  
  if(dir==0){    // sentido de z
    analogWrite(pino1_mot_b,val);
    digitalWrite(pino2_mot_b,0);
  }
  else {  // sentido de -z
    digitalWrite(pino1_mot_b,0);
    analogWrite(pino2_mot_b,val);
  }
}

void setup() {
  pinMode(pino1_mot_a,OUTPUT);
  pinMode(pino2_mot_a,OUTPUT);
  pinMode(pino1_mot_b,OUTPUT);
  pinMode(pino2_mot_b,OUTPUT);
  pinMode(pin_led,OUTPUT);
  Serial.begin(115200);
  attachInterrupt(0, encoder_a, RISING);  //pino 2
  attachInterrupt(1, encoder_b, RISING);  //pino 3
  m = 0;
}


void loop() {
  if(Serial.available()>0){
      m = Serial.parseFloat();
      /*atua_motor_a(mf(m));
      atua_motor_b(mf(m));*/
      atua_motor_a(m);
      atua_motor_b(m);
  }
  /*for(m = -255 ; m < 255 ; m+=5){
    inicio = millis();
    atua_motor_a(m);
    atua_motor_b(m);
    while(millis()-inicio<4000){*/
      t = millis();
      if(pulsos_a>p0a){
          w_a = 1000.0/(t - t_a);
          t_a = t;
          p0a = pulsos_a;
      }if(pulsos_b>p0b){
          w_b = 1000.0/(t - t_b);
          t_b = t;
          p0b = pulsos_b;
      }
      if(t-t_a>200)
        w_a = 0;
      if(t-t_b>200)
        w_b = 0;
      if(t-count>5){
        Serial.print(m);
        Serial.print("\t");
        Serial.print(w_a);
        Serial.print("\t");
        Serial.println(w_b);
        count = t;
      }
    //}
  //}
}
