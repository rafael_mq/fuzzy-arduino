/* Osciloscópio:
 *    Prd = 10.67 +-1 ms
 *    Vpp = 4.80V 
 *    Freq = 93.63Hz
 * Tempo do ATAN + leituras =~ 420 micros
 * 
 */
float theta, x, z;
const int pino_x = A0,
          pino_y = A1,
          pino_z = A2,
          pino1_mot_a = 3,
          pino2_mot_a = 4,
          pino1_mot_b = 5,
          pino2_mot_b = 6;
float PID_PV;
float e[4];
float ei;
float De;
float Kp = 1;
float Kd = 1;
float Ki = 1;
float PID_SPoff=0;
float PID_SP;
float m = 0;
const float M_MAX = 255;
const float M_MIN = -255;

void atua_motor(float valor){//recebe um valor de -255 a 255
  int val = int(valor);
  if(val > 0){
    analogWrite(pino1_mot_a,val);
    digitalWrite(pino2_mot_a,0);
    analogWrite(pino1_mot_b,val);
    digitalWrite(pino2_mot_b,0);
  }
  else if( val < 0){
    valor = abs(valor);
    analogWrite(pino1_mot_a,0);
    digitalWrite(pino2_mot_a,val);
    analogWrite(pino1_mot_b,0);
    digitalWrite(pino2_mot_b,val);
  }
  else{
    analogWrite(pino1_mot_a,0);
    digitalWrite(pino2_mot_a,0);
    analogWrite(pino1_mot_b,0);
    digitalWrite(pino2_mot_b,0);
  }
}
void setup() {
  // put your setup code here, to run once:
  e[0]=0;
  e[1]=0;
  e[2]=0;
  e[3]=0;
  ei=0;
  PID_SP = 1.318715;
  
}

void loop() {
  // put your main code here, to run repeatedly:
  delay(10);
  digitalWrite(13,1);
  x = -0.06146*analogRead(pino_x) + 20.41; 
  z = -0.06277*analogRead(pino_z) + 16.35;
  PID_PV=atan(x/z);
  digitalWrite(13,0);

  e[3]=e[2];
  e[2]=e[1];
  e[1]=e[0];
  e[0]=PID_PV-PID_SP+PID_SPoff;
  
  De = (e[0] + 3*e[1] - 3*e[2] - e[3] )/6;
  ei+=e[0];
  
  m=Kp*e[0]+Kd*De+Ki*ei;
  
  if (M_MAX<m) {
    ei-=e[0];
    m=M_MAX;
  }
  if (M_MIN>m) {
    ei-=e[0];
    m=M_MIN;      
  }
  atua_motor(m);
}
