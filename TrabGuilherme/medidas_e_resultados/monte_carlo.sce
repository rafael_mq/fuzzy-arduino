g = 9.78;
n = 5;
//colunas: val_analog aceleracao desvio
m_x = [	171.3000   g   2.4488;
	338.7123   0   4.6015254;
	488.1     -g   3.2455442];
m_y = [ 192.55333  g   3.5151382;
	359.62455  0   6.5315175;
	514.81333 -g   4.9958193];
m_z = [ 101.98     g   8.2888054;
	259.48489  0   7.0001519;
	416.42    -g   4.0521268];

X = [ m_x(1,3)*rand(n,1,"normal")+m_x(1,1) g*ones(5,1);
        m_x(2,3)*rand(n,1,"normal")+m_x(2,1) zeros(5,1);
        m_x(3,3)*rand(n,1,"normal")+m_x(3,1) -g*ones(5,1);];
Y = [ m_y(1,3)*rand(n,1,"normal")+m_y(1,1) g*ones(5,1);
        m_y(2,3)*rand(n,1,"normal")+m_y(2,1) zeros(5,1);
        m_y(3,3)*rand(n,1,"normal")+m_y(3,1) -g*ones(5,1);];
Z = [ m_z(1,3)*rand(n,1,"normal")+m_z(1,1) g*ones(5,1);
        m_z(2,3)*rand(n,1,"normal")+m_z(2,1) zeros(5,1);
        m_z(3,3)*rand(n,1,"normal")+m_z(3,1) -g*ones(5,1);];
