// Programa de identificação do acelerometro pololu mma 7361LC
/*
 *  O programa abaixo tomará várias medidas separadas por T segundos
 *  e a cada 100 amostras, enviará uma por com. serial
*/
const int pino_x = A0,
          pino_y = A1,
          pino_z = A2,
          T = 10;
int i = 0, amostras = 0, x, y, z, t_inicial, t_atual;

void setup() {
  Serial.begin(57600);   //57600, 38400 19200
  for(i=0;i<5;i++){
    Serial.print("Prepare-se : ");
    Serial.println(5-i);
    delay(100*T);
  }
  t_inicial = millis();
}

void loop() {
  // amostrando
  t_atual = millis();
  if(t_atual-t_inicial>=T){  
    i++;
    x = analogRead(pino_x);
    y = analogRead(pino_y);
    z = analogRead(pino_z);
    if(i%100==0){
      amostras++;
      Serial.print(amostras);
      Serial.print("\t");
      Serial.print(x);
      Serial.print("\t");
      Serial.print(y);
      Serial.print("\t");
      Serial.println(z);
    }
    t_inicial = millis();
  }
}
