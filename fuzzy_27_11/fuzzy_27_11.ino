#include <Arduino.h>

//Função: Gerador de funções de pertinência Fuzzy triangulares ou trapezóidais
float trimf(float x, float a, float b, float c);

//Função: Controlador Fuzzy
float speed(float ang, float der);

// Funções: Conjuntos Fuzzy de Entrada
float ang_neg(float ang);
float ang_zr(float ang);
float ang_pos(float ang);
float der_neg(float ang);
float der_zr(float ang);
float der_pos(float ang);

#define PITCH_SP 3.0
float out_fuzzy;

double compAngleY;

//Variáveis do sensor
int16_t ax, ay, az, gx, gy, gz;	//Esses vão receber os dados crus do MPU6050
double AcX,AcY,AcZ,GyY;		//Esses valores vão ser usados nos cálculos
uint32_t timer;					//Timer para calcular o atraso de cada iteração.
uint32_t temp;
#define degconvert 57.2957786	//there are like 57 degrees in a radian.

// Definição dos pinos do Arduino
const int pin_led = 13,
          pino1_mot_a = 5,
          pino2_mot_a = 6,
          pino1_mot_b = 9,
          pino2_mot_b = 10;

//Bibliotecas do I2C e do sensor MPU6050
#include "I2Cdev.h"
#include "MPU6050_6Axis_MotionApps20.h"

#if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
    #include "Wire.h"
#endif

// Definição do objeto da classe MPU6050
MPU6050 accelgyro;

void setup() {

// Setup da comunicação I2C
  #if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
      Wire.begin();
  #elif I2CDEV_IMPLEMENTATION == I2CDEV_BUILTIN_FASTWIRE
      Fastwire::setup(400, true);
  #endif

  // Inicialização dos pinos do Arduino    
  pinMode(pino1_mot_a,OUTPUT);
  pinMode(pino2_mot_a,OUTPUT);
  pinMode(pino1_mot_b,OUTPUT);
  pinMode(pino2_mot_b,OUTPUT);
  pinMode(pin_led,OUTPUT);

  Serial.begin(115200);

  // initialize device
  Serial.println("Initializing I2C devices...");
  accelgyro.initialize();

  // verify connection
  Serial.println("Testing device connections...");
  Serial.println(accelgyro.testConnection() ? "MPU6050 connection successful" : "MPU6050 connection failed");

  /*********************************************************************************************************
  * PRECISO FAZER UM CODIGO PARA CALIBRAR O SENSOR OU ADD ELE NO SETUP !!!!
  *********************************************************************************************************/

  // Definição dos valores de offset obtidos da calibração
  accelgyro.setXAccelOffset(-518);
  accelgyro.setYAccelOffset(-1095);
  accelgyro.setZAccelOffset(1604);
  accelgyro.setXGyroOffset(101);
  accelgyro.setYGyroOffset(22);
  accelgyro.setZGyroOffset(63);

  //1) Obtenção das medidas do acelerômetro
  accelgyro.getMotion6(&ax, &ay, &az, &gx, &gy, &gz);
  AcX = (double)(ax);
  AcY = (double)(ay);
  AcZ = (double)(az);
  GyY = (double)(gy);

  //2) Cálculo do pitch
  double pitch = atan2(-AcX, AcZ)*degconvert;

  //3) Define o ângulo inicial para os cáculos posteriores
  double gyroYangle = pitch;
  double compAngleY = pitch;

  //start a timer
  timer = micros();
}

void loop() {

  temp = micros();
  
  //1) Obtenção das medidas do acelerômetro
  accelgyro.getMotion6(&ax, &ay, &az, &gx, &gy, &gz);
  AcX = (double)(ax);
  AcY = (double)(ay);
  AcZ = (double)(az);
  GyY = (double)(gy);
  
  double dt = (double)(micros() - timer) / 1000000; //3) Calcula o intervalo de tempo de uma iteração em microsegundos,transforma em 
  													//segundos e invoca o resultado como um valor do tipo "double".
  timer = micros(); //Atualiza o valor da contagem de tempo.

  //3) Cálculo do pitch
  double pitch = atan2(-AcX, AcZ)*degconvert;

  //Converte o dado cru do giroscópio em graus/segundo.
  double gyroYrate = GyY/131.0;

  //FILTRO COMPLEMENTAR
  //Este filtro calcula o ângulo, quase totalmente, integrando a velocidade angular. "dt" é o tempo de amostragem equivalente ao inverso
  //da frequência de amostragem, Fs. O filtro adiciona um fator pequeno equivalente ao ângulo obtido do acelerômetro.
  //A soma dos fatores deve ter valor unitário pra se manter na escala desejada. Note que 0.95 + 0.05 = 1
  compAngleY = 0.9 * (compAngleY + gyroYrate * dt) + 0.1 * pitch; // Calculate the angle using a Complimentary filter.
  
  out_fuzzy = speed((float)compAngleY, (float)gyroYrate);
  
  temp = micros() - temp;
  Serial.print("pitch: ");
  Serial.print(compAngleY);
  Serial.print("TP: ");
  Serial.println(temp);
}

// Function for calculating aterniation to a membership function
  // x = our variable (measurment of angle or derivative)
  // a = starting point of triangle
  // b = peak of triangle
  // c = ending point of triangle
float trimf(float x,float a, float b, float c){
  float f;
  if(x<=a)
     f=0;
  else if((a<=x)&&(x<=b)) 
     f=(x-a)/(b-a);
  else if((b<=x)&&(x<=c)) 
     f=(c-x)/(c-b);
  else if(c<=x) 
     f=0;
  return f; 
}

/*******************************************************
 * Functions to compute pertinence of an input to 
 * a fuzzy set
 * ****************************************************/

float ang_neg(float ang){
    return trimf(ang, (-45.0+PITCH_SP), (-30.0+PITCH_SP), (0+PITCH_SP));
}

float ang_zr(float ang){
    return trimf(ang, (-30+PITCH_SP), (0+PITCH_SP), (30+PITCH_SP));
}

float ang_pos(float ang){
    return trimf(ang, (0+PITCH_SP), (30.0+PITCH_SP), (45.0+PITCH_SP));
}

float der_neg(float ang){
    return trimf(ang, -10, -9.9, 0);
}

float der_zr(float ang){
    return trimf(ang, -5, 0, 5);
}

float der_pos(float ang){
    return trimf(ang, 0, 9.9, 10);
}

// Function to control robot speed based on angle and
// its derivative

float speed(float ang, float der){

    float out = 0.0;
    if (ang >  60)  ang =  45;
    if (ang < -60)  ang = -45;
    if (der >  10)  der =  10;
    if (der < -10)  der = -10;
    // The output universe has its fuzzy sets on singleton
    // form for simplicity
    float negFast = -1.0;
    float negSlow = -0.5;
    float stopped =  0.0;
    float posSlow =  0.5;
    float posFast =  1.0;

    /***************************************************
     * Fuzzy Rules definition
     **************************************************/

    // Rule ANT1: IF (ang is neg AND der is neg) OR  
    // (ang is neg AND der is zr) THEN posFast
    float r1 = max(min(ang_neg(ang), der_neg(der)), 
                   min(ang_neg(ang), der_zr(der)));
    
    // Rule ANT2: IF (ang is pos AND der is pos) OR 
    // (ang is pos AND der is zr) THEN negFast
    float r2 = max(min(ang_pos(ang), der_pos(der)), 
                   min(ang_pos(ang), der_zr(der)));

    // Rule ANT3: IF (ang is neg AND der is pos) OR 
    // (ang is zr AND der is neg) THEN posSlow
    float r3 = max(min(ang_neg(ang), der_pos(der)), 
                   min(ang_zr(ang), der_neg(der)));
    
    // Rule ANT4: IF (ang is pos AND der is neg) OR 
    // (ang is zr AND der is pos) THEN negSlow
    float r4 = max(min(ang_pos(ang), der_neg(der)), 
                   min(ang_zr(ang), der_pos(der)));

    // Rule ANT5: IF (ang is zr AND der is zr) 
    // THEN stopped
    float r5 = min(ang_neg(ang), der_neg(der));

    // Deffuzyfication
    out = r1*posFast + r2*negFast + r3*posSlow + 
          r4*negSlow + r5*stopped;
           
    return out;    
}
